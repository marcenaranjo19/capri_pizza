$(document).on('ready', function() {
    $('#show-hide-passwd').on('click', function(e) {
        e.preventDefault();

        var current = $(this).attr('action');

        if (current == 'hide') {
            $(this).prev().attr('type','text');
            $(this).removeClass('fa-eye').addClass('fa-eye-slash').attr('action','show');
        }

        if (current == 'show') {
            $(this).prev().attr('type','password');
            $(this).removeClass('fa-eye-slash').addClass('fa-eye').attr('action','hide');
        }
    })
})