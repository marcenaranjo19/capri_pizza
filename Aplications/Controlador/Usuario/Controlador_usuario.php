
<?php
require_once ("../../Modelo/Conexion.php");
require_once ("../../Modelo/Usuario/Usuario.php");
require_once ("../../Modelo/Usuario/CrudUsuario.php");

$Usuario = new Usuario();
$CrudUsuario = new CrudUsuario();

if(isset($_POST["Acceder"])){//Verifico si se realizó a peticion acceder desde el formulario del Login
    //echo "Login";
    $Usuario->setCorreo($_POST["Correo"]);//Capturando el nombre del usuario y se asigna al atributo del objeto
    $Usuario->setContrasena($_POST["Contrasena"]);
    //var_dump($Usuario);
    $Usuario = $CrudUsuario->ValidarAcceso($Usuario);
    if($Usuario->getExiste()==1 && $Usuario->getIdRol()==1)
    {
        echo $Usuario->getIdUsuario();
        session_start();//Inicializacion o utilizacion de las variables de sesión
       
        header("Location:../../Vista/Admin/Principal_Admin.php");
    }
    elseif($Usuario->getExiste() == 1 && $Usuario->getIdRol() ==2)
    {
        header("Location:../../Vista/Cliente/Principal_Cliente.php");
        echo "Bienvenido: Cliente";
    }
    else{
        ?>
        <script>
                    alert ("usuario y o contraseña invalidas")
                    document.location.href="../../Vista/Login/Login.php";
        </script>
    <?php
    }

}
else{
    header("Location:../Vista/Login/Login.php");//Redireccionamiento a la pagina del Login en caso de que intenten ingresar desde la vista controladorUsuario
}

?>