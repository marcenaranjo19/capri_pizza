<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
	<link rel="stylesheet" href="../../../Public/Estilos/semantic.min.css">
	<link rel="stylesheet" href="../../../Public/Estilos/Principal_admin.css">
	
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" >
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<script src="../../../js/jquery-3.1.1.min.js"></script>
	<script src="../../../js/semantic.min.js"></script>	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>
<body>

<div class="topnav"> 
	<a class="logotipo" href="#home">  <i class="fas fa-bars"></i> <b>La Caprichosa</b></a>
    <a href="../../Modelo/CerrarSesion.php">Cerrar Sesión</a>
    <a href="#configuracion"> <i class="fas fa-cogs"></i> Configuración</a>
    <a href="#notificaciones"><i class="far fa-bell ui red circular label">2</i></a>
	<div class="ui visible sidebar"></div>
</div>

    <div class="menu_abajo">
        <p class="inf_menu_abajo">Inicio    /    Administrador    /    Menú Principal</p>
    </div></div>

<div class="fondo_menu">
    <div class="contenedor-menu">
    <a class="logotipo" href="#home">  <img src="../../../Public/Img/icono.png" alt="Logo de Capri-Pizza" title="Imagen hecha por desarrolladores Capri-Pizza."width="50" height="50"> <b>Capri-Pizza </b></a>
        <p class="user" ><i class=" icon_usuario fas fa-user-circle" ></i> Administrador 
		<br> <br> <a class="ui green empty circular label"></a> Online

        </p> 
    <h3 class="title_menu">Menú Principal</h3>
		<ul class="menu">
			<li><a href="#"><i class="icono izquierda fa fa-home"></i>Inicio</a></li>
			<li><a href="#"><i class="icono izquierda fas fa-book-reader"></i></i>Catálogo<i class="icono derecha fa fa-chevron-down"></i></a>
				<ul>
					<li><a href="#Promociones">Promociones</a></li>
					<li><a href="https://www.google.com">Catálogo</a></li>
				</ul>
			</li>
			<li><a href="#"><i class="icono izquierda fas fa-shopping-basket"></i>Gestión de Pedidos<i class="icono derecha fa fa-chevron-down"></i></a>
				<ul>
					<li><a href="../../../EstadoPedido/Vista/ListarEstadosPedido.php">Estado de Pedidos</a></li>
					<li><a href="#">Historial de Pedidos</a></li>
					<li><a href="../../../Pedido/Index.php">Reporte de Pedidos</a></li>
					<li><a href="#">Insumos</a></li>
				</ul>
			</li>
			<li><a href="#"><i class="icono izquierda fas fa-user-circle"></i>Gestión de Usuarios<i class="icono derecha fa fa-chevron-down"></i></a>
				<ul>
					<li><a href="../../../Cliente/Vista/ListarClientes.php">Usuarios</a></li>
				</ul>
			</li>
			
		</ul>
    </div>
</div>
</body>
<script src="../../../Public/Js/Principal_admin.js"></script>
</html>


