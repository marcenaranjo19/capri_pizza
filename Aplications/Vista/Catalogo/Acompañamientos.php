<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>La Caprichosa</title>
    <link rel="icon" type="image/png" href="../../../Public/Img/icono.png" />
    <link rel="stylesheet" href="../../../Public/Estilos/Header.css">	
    <link rel="stylesheet" href="../../../Public/Estilos/Index.css">	
    <link rel="stylesheet" href="../../../Public/Estilos/Footer.css">	
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
</head>
<body>
<body>
<div class="topnav"> 
<a class="logotipo" href="#logotipo">  <img src="../../../Public/Img/icono.png" alt="Logo de Capri-Pizza" 
title="Imagen hecha por desarrolladores Capri-Pizza."width="60" height="60"> <b class="nombre-empresa">La Caprichosa </b>
  <a href="#Registro" >Registrarme</a>
  <a href="../Login/Login.php">Ingresar</a>
  <a href="#Personaliza_tu_Pizza">Personaliza tu Pizza</a>
</div>
    <div class="menu_abajo">
        <a href="#Contáctanos">Contáctanos</a>
      <a href="#Sobre_Nosotros">Sobre Nosotros</a>
      <a href="#mi_pedido">Mi pedido</a>
      <a href="Bebidas.php">Bebidas</a>
      <a href="Acompañamientos.php">Acompañamientos</a>
      <a href="Pizzas.php">Pizzas</a>
      <a class="active" href="../../Index.php">Inicio</a>
    </div></div>

<br> <br>
<!-- SESIÓN 1-->
<h1 class="title-bloque" align="center" >ACOMPAÑAMIENTOS</h1>
  <div class="con-cajas">
    <div class="cajas">
    <div class="">     
    <img src="../../../Public/Img/acomp1.jpg" alt="Imagen" align="center" style="height:170px; padding:16px" />
    </div>
    <h3 class="title-product" align="center">Palitos de queso</h3>
    <h3 class="precio" align="center">Precio: $9999</h3>
    <button  class="btn_pedir" align="center" >Pedir Ahora !</button>
  </div>
    <div class="cajas">
    <div class="">     
    <img src="../../../Public/Img/acomp2.jpg" alt="Imagen" align="center" style="height:170px; padding:16px" />
    </div>
    <h3 class="title-product" align="center">Rollo de carne</h3>
    <h3 class="precio" align="center">Precio: $9999</h3>
    <button  class="btn_pedir" align="center" >Pedir Ahora !</button>
  </div>

    <div class="cajas">
    <div class="" >     
    <img src="../../../Public/Img/acomp3.jpg" alt="Imagen" align="center" style="height:170px; padding:16px" />
    </div>
    <h3 class="title-product" align="center">Hojaldre</h3>
    <h3 class="precio" align="center">Precio: $9999</h3>
    <button  class="btn_pedir" align="center" >Pedir Ahora !</button>
  </div>
 
    <div class="cajas">
    <div class="">     
    <img src="../../../Public/Img/acomp4.jpg" alt="Imagen" align="center" style="height:170px; padding:16px" />
    </div>
    <h3 class="title-product" align="center">Mini pan de guayaba</h3>
    <h3 class="precio" align="center">Precio: $9999</h3>
    <button  class="btn_pedir" align="center" >Pedir Ahora !</button>
  </div>
  </div>

  <!-- SESIÓN 2-->
  <div class="con-cajas">
    <div class="cajas">
    <div class="">     
    <img src="../../../Public/Img/acomp1.jpg" alt="Imagen" align="center" style="height:170px; padding:16px" />
    </div>
    <h3 class="title-product" align="center">Palitos de queso</h3>
    <h3 class="precio" align="center">Precio: $9999</h3>
    <button  class="btn_pedir" align="center" >Pedir Ahora !</button>
  </div>
    <div class="cajas">
    <div class="">     
    <img src="../../../Public/Img/acomp2.jpg" alt="Imagen" align="center" style="height:170px; padding:16px" />
    </div>
    <h3 class="title-product" align="center">Rollo de carne</h3>
    <h3 class="precio" align="center">Precio: $9999</h3>
    <button  class="btn_pedir" align="center" >Pedir Ahora !</button>
  </div>

    <div class="cajas">
    <div class="" >     
    <img src="../../../Public/Img/acomp3.jpg" alt="Imagen" align="center" style="height:170px; padding:16px" />
    </div>
    <h3 class="title-product" align="center">Hojaldre</h3>
    <h3 class="precio" align="center">Precio: $9999</h3>
    <button  class="btn_pedir" align="center" >Pedir Ahora !</button>
  </div>
 
    <div class="cajas">
    <div class="">     
    <img src="../../../Public/Img/acomp4.jpg" alt="Imagen" align="center" style="height:170px; padding:16px" />
    </div>
    <h3 class="title-product" align="center">Mini pan de guayaba</h3>
    <h3 class="precio" align="center">Precio: $9999</h3>
    <button  class="btn_pedir" align="center" >Pedir Ahora !</button>
  </div>
  </div>
  <!-- SESIÓN 3-->
  <div class="con-cajas">
    <div class="cajas">
    <div class="">     
    <img src="../../../Public/Img/acomp1.jpg" alt="Imagen" align="center" style="height:170px; padding:16px" />
    </div>
    <h3 class="title-product" align="center">Palitos de queso</h3>
    <h3 class="precio" align="center">Precio: $9999</h3>
    <button  class="btn_pedir" align="center" >Pedir Ahora !</button>
  </div>
    <div class="cajas">
    <div class="">     
    <img src="../../../Public/Img/acomp2.jpg" alt="Imagen" align="center" style="height:170px; padding:16px" />
    </div>
    <h3 class="title-product" align="center">Rollo de carne</h3>
    <h3 class="precio" align="center">Precio: $9999</h3>
    <button  class="btn_pedir" align="center" >Pedir Ahora !</button>
  </div>

    <div class="cajas">
    <div class="" >     
    <img src="../../../Public/Img/acomp3.jpg" alt="Imagen" align="center" style="height:170px; padding:16px" />
    </div>
    <h3 class="title-product" align="center">Hojaldre</h3>
    <h3 class="precio" align="center">Precio: $9999</h3>
    <button  class="btn_pedir" align="center" >Pedir Ahora !</button>
  </div>
 
    <div class="cajas">
    <div class="">     
    <img src="../../../Public/Img/acomp4.jpg" alt="Imagen" align="center" style="height:170px; padding:16px" />
    </div>
    <h3 class="title-product" align="center">Mini pan de guayaba</h3>
    <h3 class="precio" align="center">Precio: $9999</h3>
    <button  class="btn_pedir" align="center" >Pedir Ahora !</button>
  </div>
  </div>
  
  

  <!--PIE DE PAGINA -->
  <footer class="footer-distributed">
 
 <div class="footer-left">

     <h3>Sobre <span>La Caprichosa</span></h3>

<div class="footer-icons">
         <a href="#"><i class="fa fa-facebook"></i></a>
         <a href="#"><i class="fa fa-twitter"></i></a>
         <a href="#"><i class="fa fa-instagram"></i></a>
         
     </div>
<p class="footer-company-name">© 2020 La Caprichosa.</p>
 </div>

 <div class="footer-center">
     <div>
<i class="fa fa-map-marker"></i>
<span class="span" >Encuentranos aquí</span>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63463.30283669667!2d-75.59949687066454!3d6.203363450313642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e4428299a208b6f%3A0xdf22185e8e9c170d!2sEl%20Poblado%2C%20Medell%C3%ADn%2C%20Medellin%2C%20Antioquia!5e0!3m2!1sen!2sco!4v1591758913799!5m2!1sen!2sco" width="400" height="200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
</div>

 <div class="footer-right">
     <p class="footer-company-about">
<span>Preguntas Frecuentes</span>
 En este espacio podrás ver las preguntas frecuentes y dejar tu 
 petición, queja, sugerencia o reclamo, tu opinión es muy importante.
    <button class="button_mensaje">Enviar Mensaje</button>
 </div>
</footer>

</body>
</html>