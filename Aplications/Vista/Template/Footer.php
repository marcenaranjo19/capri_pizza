<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>footer</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" >
 <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200&display=swap" rel="stylesheet">
 <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> 
<link rel="stylesheet" href="Estilos/Footer.css">	
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
</head>
<body>
    
    
<footer class="footer-distributed">
 
 <div class="footer-left">

     <h3>Sobre <span>La Caprichosa</span></h3>

<div class="footer-icons">
         <a href="#"><i class="fa fa-facebook"></i></a>
         <a href="#"><i class="fa fa-twitter"></i></a>
         <a href="#"><i class="fa fa-instagram"></i></a>
         
     </div>
<p class="footer-company-name">© 2020 La Caprichosa.</p>
 </div>

 <div class="footer-center">
     <div>
<i class="fa fa-map-marker"></i>
<span class="span" >Encuentranos aquí</span>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63463.30283669667!2d-75.59949687066454!3d6.203363450313642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e4428299a208b6f%3A0xdf22185e8e9c170d!2sEl%20Poblado%2C%20Medell%C3%ADn%2C%20Medellin%2C%20Antioquia!5e0!3m2!1sen!2sco!4v1591758913799!5m2!1sen!2sco" width="400" height="200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
</div>

 <div class="footer-right">
     <p class="footer-company-about">
<span>Preguntas Frecuentes</span>
 En este espacio podrás ver las preguntas frecuentes y dejar tu 
 petición,quejas,sugerencias o reclamos, tu opinión es muy importante.
    <button class="button_mensaje">Enviar Mensaje</button>
 </div>
</footer>

</body>
</html>