<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login</title> 
    <link rel="icon" type="image/png" href="../../../Public/Img/icono.png" />
    <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0">
   
    <!--Link para los iconos de la pagina Font Awesome-->
        
    <link rel="stylesheet" href="../../../Public/Estilos/Header.css">
    <link rel="stylesheet" href="../../../Public/Estilos/Login.css">
    <link rel="stylesheet" href="../../../Public/Estilos/Footer.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" >
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200&display=swap" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> 


    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <!--Bootsrap 4 CDN-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!--Fontawesome CDN-->
	<link  integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

</head>

<body>
<div class="topnav"> 
<a class="logotipo" href="#home">  <img src="../../../Public/Img/icono.png" alt="Logo de Capri-Pizza" 
title="Imagen hecha por desarrolladores Capri-Pizza."width="60" height="60"> <b class="nombre-empresa">La Caprichosa </b></a>

</div>
<div class="menu_abajo">
    <a href="#Contactanos">Contáctanos</a>
  <a href="#Sobre_Nosotros">Sobre Nosotros</a>
  <a href="../Catalogo/Bebidas.php">Bebidas</a>
  <a href="../Catalogo/Acompañamientos.php">Acompañamientos</a>
  <a href="../Catalogo/Pizzas.php">Pizzas</a>
  <a class="active" href="../../Index.php">Inicio</a>
</div></div>
     
    <div class="contenedor">
	<div class="d-flex justify-content-center h-100">
		<div class="formulario">
			<div class="icono">
			<div class="log" align="center"><img src="../../../Public/Img/icono.png" width="130" height="130" > </div>
			</div>
			<div class="card-body">
				<form id="Formulario" action="../../Controlador/Usuario/Controlador_Usuario.php" method="post">
					<!--Input correo-->
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-user"></i></span>
						</div>
						<input type="text" class="form-control" placeholder="Correo Electrónico" name="Correo" id="Correo">	
					</div>
						<!--Input contraseña-->
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" class="form-control" placeholder="Contraseña" name="Contrasena" id="Contrasena">
							<!--visualizar contraseña-->
						<span id="show-hide-passwd" action="hide" class="input-group-text fa fa-eye"></span> 
					</div>

					<p class="p1"> <b> ¿Olvidaste tu contraseña? </b></p>
					<div class="form-group">
						<button type="submit" id="Acceder" class="button" name="Acceder" value="Acceder">Ingresar</button> 
					</div>
				
					<p class="p2"> ¿No tienes una cuenta? <a class="link" href="registro.html">Registrate</a> </p> 				   
				</form>
			</div>
		</div>
	</div>
</div>


<!--PIE DE PAGINA-->
<footer class="footer-distributed">
 
 <div class="footer-left">

     <h3>Sobre <span>La Caprichosa</span></h3>

<div class="footer-icons">
         <a href="#"><i class="fa fa-facebook"></i></a>
         <a href="#"><i class="fa fa-twitter"></i></a>
         <a href="#"><i class="fa fa-instagram"></i></a>
         
     </div>
<p class="footer-company-name">© 2020 La Caprichosa.</p>
 </div>

 <div class="footer-center">
     <div>
<i class="fa fa-map-marker"></i>
<span class="span" >Encuentranos aquí</span>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63463.30283669667!2d-75.59949687066454!3d6.203363450313642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e4428299a208b6f%3A0xdf22185e8e9c170d!2sEl%20Poblado%2C%20Medell%C3%ADn%2C%20Medellin%2C%20Antioquia!5e0!3m2!1sen!2sco!4v1591758913799!5m2!1sen!2sco" width="400" height="200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
</div>

 <div class="footer-right">
     <p class="footer-company-about">
<span>Preguntas Frecuentes</span>
 En este espacio podrás ver las preguntas frecuentes y dejar tu 
 petición,quejas,sugerencias o reclamos, tu opinión es muy importante.
    <button class="button_mensaje">Enviar Mensaje</button>
 </div>
</footer>


</body>
<script src="../../../Public/Js/Login.js">
</script>

</html>




