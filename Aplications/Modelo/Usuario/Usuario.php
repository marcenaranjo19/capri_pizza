<?php
class Usuario{
    private $IdUsuario;
    private $Correo;
    private $Contrasena;
    private $IdRol;
    private $IdEstado;
    private $Existe;

    public function __construct(){     
    }

    public function setIdUsuario($IdUsuario){
        $this->IdUsuario = $IdUsuario;
    }
    public function getIdUsuario(){
        return $this->IdUsuario;
    }

    public function setCorreo($Correo){
        $this->Correo = $Correo;
    }
    public function getCorreo(){
        return $this->Correo;
    }

    public function setContrasena($Contrasena){
        $this->Contrasena = $Contrasena;
    }
    public function getContrasena(){
        return $this->Contrasena;
    }

    public function setIdRol($IdRol){
        $this->IdRol = $IdRol;
    }
    public function getIdRol(){
        return $this->IdRol;
    }

    public function setIdEstado($IdEstado){
        $this->IdEstado = $IdEstado;
    }
    public function getIdEstado(){
        return $this->IdEstado;
    }

    public function setExiste($Existe){
        $this->Existe = $Existe;
    }
    public function getExiste(){
        return $this->Existe;
    }
}

?>