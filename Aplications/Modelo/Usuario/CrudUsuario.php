<?php
class CrudUsuario{

    public function ValidarAcceso($Usuario)
    {
        $Db = Db::Conectar();//Conexión a la base de datos
        $Sql = $Db->prepare('SELECT * FROM usuarios WHERE Correo=:Correo AND Contrasena=:Contrasena AND IdEstado=1');
        //Capturar datos del objeto con el bindValue
        $Sql->bindValue('Correo',$Usuario->getCorreo());
        $Sql->bindValue('Contrasena', md5($Usuario->getContrasena()));
        
        try
        {
        $Sql->execute();//Ejecutar la consultar.
        }
        catch(Exception $e)
        {
            echo "<br>".$e->getMessage()."<br>";
        }
        $MiUsuario = new Usuario();//Crear el objeto a devolver al controlador.
        if($Sql->rowCount() > 0){//Si el numero de registros de la consulta es mayor a 0 el Usuario existe. 
        //echo "Existe";
        $Usuario = $Sql->fetch();//Almacenar datos arrojados en la consulta.
        $MiUsuario->setIdUsuario($Usuario['IdUsuario']);
        $MiUsuario->setCorreo($Usuario['Correo']);
        $MiUsuario->setIdRol($Usuario['IdRol']);
        $MiUsuario->setExiste(1);
        }
        else{//Caso contrario no existe el usuario.
            //echo "No esxiste";
            //echo $Usuario->getCorreo();
            $MiUsuario->setExiste(0); //Retorno 0 es decir que no existe el usuario
        }
        return $MiUsuario; //Retornar el objeto 
    }
}

?>